# La storia dei nicks

C'è poco da fare: l'antico web era una rete di pesudonimi. E la ragione era semplice: l'antico web era popolato da gente che conosceva bene lo strumento sotto le loro mani. Erano informatici, hacker (nel senso buono del termine), appassionati di tecnologia (geek) e comunque addetti ai lavori.
